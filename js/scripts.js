function changecolor(){
        var style = getComputedStyle(document.body);
        if(style.getPropertyValue('--blue') == "#565656"){
            $(document.documentElement).css("--gold","gold");
            $(document.documentElement).css("--blue","#1f4d7d");
            $(document.documentElement).css("--white","whitesmoke");
            $(document.documentElement).css("--qr",'url("../img/QRCode.png")');
            $(document.documentElement).css("--pastille","#4f7dad");
            $(document.documentElement).css("--button","#26a69a");
            $('#change').text("Couleur");
        }else{
            $(document.documentElement).css("--blue","#565656");
            $(document.documentElement).css("--white","white");
            $(document.documentElement).css("--gold","white");
            $(document.documentElement).css("--qr",'url("../img/QRCode_black.png")');
            $(document.documentElement).css("--pastille","#969696");
            $(document.documentElement).css("--button","#969696");
            $('#change').text("Monochrome");
        }
    }

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") {
    hidden = "hidden";
    visibilityChange = "visibilitychange";
    visibilityState = "visibilityState";
} else if (typeof document.mozHidden !== "undefined") {
    hidden = "mozHidden";
    visibilityChange = "mozvisibilitychange";
    visibilityState = "mozVisibilityState";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
    visibilityState = "msVisibilityState";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
    visibilityState = "webkitVisibilityState";
}

$(document).ready(function(){
    
    var stop = 1;
    /**i18next
      .use(i18nextXHRBackend)
      .init({
        whitelist: ['en', 'fr'],
        lng: 'fr',
        loadPath: '../locales/{{lng}}/translation.json'
      }, function(err, t) {
        jqueryI18next.init(i18next, $);
        //$('#expp').css("height","12.7cm !important")
        $("body").localize();
      });
    function translate(){
      if(i18next.language == 'en')
        i18next.changeLanguage('fr',(err, t) => {
            $("body").localize();
            link();
        });
      else i18next.changeLanguage('en',(err, t) => {
            $("body").localize();
            link();
        });
    }**/
    function stopPulse(){
        $(".btn-floating").removeClass('pulse');
        clearInterval(stop);
    }
    function showBars(){
        $(".skills").find(".progress-bar").animate({'width': '300px'},{duration: 2000});
    }
    function showCols(){
        $(".scale-out").each(function(){
            $(this).removeClass('scale-out');
        });
    }
    function removeScaleClasses($card){
        $card.removeClass('scale-out');
    }

    function pdf(){
        var filename = 'CV_Eddie_BILLOIR';
        if($(":root").css("--blue") == "#565656"){
            filename += '_PRINTER';
        }
        if(i18next.language == 'en'){
            filename += '_EN';
        }
        $(location).attr('href',filename+'.pdf');
    }
    function mindmap(){
        if(i18next.language == 'fr'){
            $(location).attr('href','moimeme.html');
        }else{
            $(location).attr('href','myself.html');
        }
    }
    function link(){
      $('.timeline').each(function(){
        var h = $(this).find(".contenu").last().offset().top - $(this).offset().top + 2;
        $(this).find(".vertical-bar").first().css("height",h+"px");
      }); 
    }
    function shake(){
        $( "#buttons" ).effect( "shake" );
        stop = setInterval(function(){
            if(!document[hidden])$( "#buttons" ).effect( "shake" );
        },5000);
    }
    showCols();
    setTimeout(showBars,20);
    //$("#translator").on("click",translate);
    $('#button-pdf').on("click",pdf);
    $("#mindmap").on("click",mindmap);
    $(".btn-floating").on("click mouseenter mouseleave",stopPulse);
    link();
    shake();
    //$(".btn-large").on("click",pdf);
})
